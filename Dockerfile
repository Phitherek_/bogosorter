FROM crystallang/crystal:latest-alpine

WORKDIR /usr/src/app

COPY . .

RUN shards --production build bogosorter --release 
CMD ["./bin/bogosorter"]
