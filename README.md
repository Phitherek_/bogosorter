# bogosorter

An implementation of Bogo Sort in Crystal

## Installation

`shards build bogosorter`

## Usage

`./bin/bogosorter`

OR

`BOGOSORTER_ARRAY_SIZE=size ./bin/bogosorter`

## Development

1. Clone the repo
2. Run `shards`
3. Add code
4. Run `./bin/ameba` and ensure that it's green
5. Run `shards build bogosorter` and check for compiler errors
6. If everything is good you can proceed with commiting changes

## Contributing

1. Fork it (<https://gitlab.com/Phitherek_/bogosorter/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Follow instructions from Development
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to the branch (`git push origin my-new-feature`)
6. Create a new Merge Request

## Contributors

- [Phitherek_](https://gitlab.com/Phitherek_) - creator and maintainer
