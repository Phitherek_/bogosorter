module Bogosorter
  class Generator
      def initialize(size : UInt16)
        @size = size
      end

      def call
        used = [] of UInt16
        generated = [] of UInt16
        @size.times do
          number = rand(0...20_000).floor.to_u16
          while used.includes?(number)
            number = rand(0...20_000).floor.to_u16
          end
          generated << number
          used << number
        end
        generated
      end
  end
end
