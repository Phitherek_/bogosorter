module Bogosorter
  class Sorter
    def initialize(array : Array(UInt16))
      @array = array
    end

    def call
      @array = @array.shuffle
    end

    def sorted?
      return false if @array.nil?

      sorted = true
      0.upto(@array.size-2) do |i|
       if @array[i] > @array[i+1]
         sorted = false
         break
       end
      end
      sorted
    end

    def array
      @array
    end
  end
end
