require "./bogosorter/generator"
require "./bogosorter/sorter"

module Bogosorter
  VERSION = "0.1.0"

  puts "Running Bogosorter v. #{VERSION}"
  puts

  if ENV.has_key?("BOGOSORTER_ARRAY_SIZE")
    size = ENV["BOGOSORTER_ARRAY_SIZE"].to_u16
    puts "Array size: #{size}"
  else
    print "Array size: "
    size = STDIN.gets
    exit(1) if size.nil?
    size = size.chomp.to_u16
  end
  puts

  array = Bogosorter::Generator.new(size).call
  puts "Initial array: #{array}"
  puts
  sorter = Bogosorter::Sorter.new(array)
  start = Time.utc
  iterations = 0_u128
  while !sorter.sorted?
      iterations += 1
      sorter.call
  end
  time_elapsed = Time.utc - start
  puts "Sorted!\tIterations: #{iterations}\tTime elapsed: #{time_elapsed.hours}h#{time_elapsed.minutes}m#{time_elapsed.seconds}s"
  puts
  puts "Sorted array: #{sorter.array}"
end
